import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})


export class HomeComponent implements OnInit {

  command = window.localStorage.getItem('command');



  constructor() { }

  ngOnInit(): void { }

  test(){
    const { exec } = require('child_process');
    exec('node --version', {silent:true}, (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`);
        return;
      }
      console.log(`stdout: ${stdout}`);
      console.error(`stderr: ${stderr}`);
    });
  }

  test2(){
    const { exec } = require('child_process');
    exec(this.command, (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`);
        return;
      }
      console.log(`stdout: ${stdout}`);
      console.error(`stderr: ${stderr}`);
    });
  }

  save(){
    window.localStorage.setItem('command',this.command);
  }
}
